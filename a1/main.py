from Map import Map_Obj as SamfundetMap

from math import sqrt, pow
from queue import PriorityQueue as pq
from dataclasses import dataclass, field
from typing import Any

samfundet = SamfundetMap(task=3)

samfundet.fill_critical_positions(1)

int_map, str_map = samfundet.get_maps()


def h(start, goal):
    start_x, start_y = start
    goal_x, goal_y = goal
    # estimate the distance by calculating the distance between two points
    return sqrt(pow(goal_x - start_x, 2) + pow(goal_y - start_y, 2))


@dataclass(order=True)
class set_item:
    priority: float
    item: Any = field(compare=False)
    g: float = field(compare=False)
    h: float = field(compare=False)
    parent: Any = field(compare=False)


def get_value(coordinate):
    return int_map[coordinate[0]][coordinate[1]]


directions = [
    [0, 1], [1, 0], [0, -1], [-1, 0],
    # [-1, -1], [1, 1], [-1, 1], [1, -1]
]


def go_direction(coordinate, direction):
    return [coordinate[0] + direction[0], coordinate[1] + direction[1]]


def detect_obsticale(coordinate):
    x, y = coordinate
    # if x < 0 or x >= 38 or y >= 47 or y < 0:
    #     return False
    return get_value(coordinate) == -1


def get_score(node):
    return node.g + node.h


def find_next(open_set, coordinate):
    for item in open_set:
        if coordinate == item.item:
            return item
    return None


def in_queue(coordinate, queue):
    i = 0
    for item in queue.queue:
        if coordinate == item.item:
            return i
        ++i
    return -1


def a_star(start, goal):
    open_set = pq()
    closed_set = []

    open_set.put(set_item(1.0, start, -1.0, 1.0, None))

    while not open_set.empty():
        current = open_set.get()

        if current.item == goal:
            break

        closed_set.append(current.item)

        i = 0
        for direction in directions:
            new_coordinate = go_direction(current.item, direction)
            if detect_obsticale(new_coordinate):
                continue
            if new_coordinate in closed_set:
                continue
            total_cost = current.g

            index = in_queue(new_coordinate, open_set)

            next = None
            heu = h(new_coordinate, goal) + \
                samfundet.get_cell_value(new_coordinate)

            if index == -1:
                next = set_item(total_cost + heu,
                                new_coordinate, total_cost, heu, current)
                open_set.put(next)
            elif total_cost < open_set.queue[index].g:
                next = open_set.queue[index]
                next.parent = current
                next.g = total_cost
                next.priority = heu + next.g
            ++i

    path = []
    while current is not None:
        path.append(current.item)
        current = current.parent

    return path


path = a_star(samfundet.get_start_pos(), samfundet.get_goal_pos())

for c in path:
    samfundet.replace_map_values(c, 5, samfundet.get_goal_pos())

# samfundet.print_map(int_map)
samfundet.show_map()
